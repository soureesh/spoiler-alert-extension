submit_btn = document.getElementById("submitBtn")
add_words = document.getElementById("addKW")
remove_words = document.getElementById("removeKW")
console.log("before sending message");
test = document.getElementById("test");

chrome.storage.sync.get(['spoilerWords'],function(result){
    test.innerText = result.spoilerWords;
});

submit_btn.onclick = submit_fun;

function submit_fun(){
    chrome.storage.sync.get(['spoilerWords'], function(result) {
        console.log('Initial Spoilers =  ' + result.spoilerWords); 
        new_list = add_words.value.split(",")

        function removeBlanks(s){
            return s.length > 0;
        }
        new_list = new_list.filter(removeBlanks)

        spoilerWordList =  [...new_list,...result.spoilerWords]

        console.log('Value after addition ' + spoilerWordList);

        remove_list = remove_words.value.split(",")
        for(var r of remove_list){
            index = spoilerWordList.indexOf(r);
            if(index > -1){
                spoilerWordList.splice(index,1)
            }
        }
        console.log('Value after removing words ' + spoilerWordList);

        //set final spoiler list
        chrome.storage.sync.set({spoilerWords: spoilerWordList}, function() {
            console.log("Spoiler Words are set in popup as ",spoilerWordList);
          });
        
          test.innerText = spoilerWordList;
      });
    
}

