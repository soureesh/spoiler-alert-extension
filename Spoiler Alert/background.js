//sanity check
console.log("Background Script log ")

//set deafult keywords
chrome.runtime.onInstalled.addListener(function() {
    chrome.storage.sync.set({spoilerWords: ["Sachin Tendulkar","Sachin","Tendulkar"]}, function() {
      console.log("Spoiler Words are set to defaults");
    });
});

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        console.log("background.js got a message")
        console.log(request.text);
        console.log("sent from: ",sender);
        sendResponse("bar");
 
        chrome.notifications.create(
            'Spoiler Alert Notification',{   
            type: 'basic', 
            iconUrl: 'icon.png', 
            title: "Spoiler Alert", 
            message: "There are " + request.spoiler_count + " spoilers on this webpage! "  
            },
        
        //for other types
        //function() {} 
        );
    }
)
