//sanity check
console.log("Checking extension!");

//get keywords from storage, find and hide spoilers 
chrome.storage.sync.get(['spoilerWords'], function(result) {
    console.log('Value currently is ' + result.spoilerWords);
    spoilerWordList = result.spoilerWords;

    var num_of_spoilers = 0;

    var allTags = [...document.body.getElementsByTagName('*')];

    console.log("KWs : = ",...spoilerWordList)
    
    function findTextNodes(){
            for(var tag of allTags){
                childern = tag.childNodes;
                for(var child of childern){
                    if(child.nodeType == 3){
                        replaceText(child);
                    }
                }
            }   
    }

    function replaceText (node) {
        for(var spoiler of spoilerWordList){
            let value = node.nodeValue;
            if(value.includes(spoiler) ){
                value = value.replace(spoiler,"Blocked - Spoiler Alert!");
                node.nodeValue = value;
                num_of_spoilers++;
            }
        }
    }
    
    findTextNodes();
    let msg = {
        text : "Spoiler",
        spoiler_count : num_of_spoilers
    }
    //send message to notify user about spoilers
    chrome.runtime.sendMessage(msg,function (response) {
            console.log(response);
        }
    );



});
