# Spoiler Alert Extension

This extension will save you from spoilers. It is easy to use and supports features like notification on finding spoilers, hiding spoilerwords on webpages, addition and removal of spoiler keywords.

<br />

### To use the extension :
```
1. Download code from this repo.
```
```
2. Go to chrome://extensions and enable developer mode.
```
```
3. Uplode the folder containing this code by clicking on 'load unpacked'
```

